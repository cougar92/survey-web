import {HttpClient, json} from 'aurelia-fetch-client';

export class TakeSurvey {

  httpClient = new HttpClient();

  survey = {}

  surveyAnswer = {
    evaluator: 'Diego Arguelles',
    evaluated: 'Alexiz Zambrano',
    questionnaireType: 'Docente',
    questions: []
  }

  activate(params) {
    console.log('Activate')
    this.httpClient.fetch('http://192.168.43.242:3000/api/questionnaire/' + params.idsurvey)
      .then(response => response.json())
      .then(response => this.survey = response);
  }

  saveSurvey() {
    this.survey.questions.map(question => {
      let answer = {
        id: question._id,
        text: question.text,
        questionType: question.questionType,
        answer: this.getAnswerValue(question._id, question.questionType)
      }
      console.log(answer);
    });
  }

  getAnswerValue(questionId, questionType) {
    let answer = ''
    if (questionType === 'text') {
      answer = document.getElementById('text' + questionId).value;
    } else if (questionType === 'checkbox') {
      console.log('Checkbox')
      let checkedValue = document.querySelector('.checkbox' + questionId).value;
      answer = checkedValue;
    }
    console.log(answer);
  }

}
