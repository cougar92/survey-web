import {computedFrom} from 'aurelia-framework';
import {HttpClient, json} from 'aurelia-fetch-client';


export class NewSurvey {

  owner = 'Diego Arguelles';
  type = '';

  questions = [];

  labelValue;
  fileInput;
  isManualCreation = false;
  answerOptions = [];
  currentAnswerOption;
  answerType = '';
  showRadioOptions = false;
  showCheckboxOptions = false;
  showInputOptionText = false;
  textQuestion = '';

  httpClient = new HttpClient();


  @computedFrom('fileInput.files')
  get selectedFile() {
    if (this.fileInput.files) {
      console.log(this.fileInput.files);
      return this.fileInput.files.length > 0 ? this.fileInput.files[0] : '';
    }
  }

  markAnswerType() {
    if (this.answerType === 'text') {
      this.showInputOptionText = false;
    }
    else if (this.answerType === 'radio') {
      this.showInputOptionText = true;
      this.showRadioOptions = true;
      this.showCheckboxOptions = false;
    }
    else if (this.answerType === 'checkbox') {
      this.showInputOptionText = true;
      this.showRadioOptions = false;
      this.showCheckboxOptions = true;
    }
  }

  switchToManually() {
    this.isManualCreation = true;
  }

  returnToMainOptions() {
    this.isManualCreation = false;
  }

  addAnswerOption() {
    this.answerOptions.push(this.currentAnswerOption);
    this.currentAnswerOption = '';
  }

  addQuestion() {

    if (this.answerType === '') {
      // TODO show toast
      return false;
    }

    this.questions.push(
      {
        text: this.textQuestion,
        questionType: this.answerType,
        options: this.answerOptions
      }
    );

    this.textQuestion = '';
    this.answerType = '';
    this.answerOptions = [];
    this.showRadioOptions = false;
    this.showCheckboxOptions = false;
    this.showInputOptionText = false;
  }

  saveSurvey() {
    console.log('saving')
    console.log(this.type)
    console.log(JSON.stringify(this.questions));

    let survey = {
      questionnaireType: this.type,
      questions: this.questions
    };

    console.log(JSON.stringify(survey));

    this.httpClient.fetch('http://192.168.43.242:3000/api/questionnaire', {
      method: 'post',
      body: json(survey)
    }).then(response => response.json())
      .then(response => console.log(response));
  }

}
